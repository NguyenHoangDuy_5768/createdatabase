﻿namespace NganHang.Models
{
    public class Report
    {
        public int Id { get; set; }
        public string ReportName { get; set; }
        public DateTime ReportDate { get; set; }

        // Navigation properties
        public ICollection<Transactions> Transactions { get; set; }
        public ICollection<Account> Accounts { get; set; }
    }
}

