﻿namespace NganHang.Models
{
    public class Logs
    {
        public int LogsID { get; set; }
        public int TransactionalID { get; set; }
        public DateTime LoginDate { get; set; }
        public TimeSpan LoginTime { get; set; }
        public virtual Transactions Transactions { get; set; }
    }
}
