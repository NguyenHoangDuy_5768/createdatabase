﻿using Microsoft.EntityFrameworkCore;
using NganHang.Data;
using System.Composition;
using System.Transactions;

namespace NganHang.Models
{
    public class BankingContext(DbContextOptions<ApplicationDbContext> options) : DbContext(options)
    {
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Report> Reports { get; set; }
        public DbSet<Transaction> Transactions { get; set; }
        public DbSet<Logs> Logs { get; set; }
        public DbSet<Account> Accounts { get; set; }
    }
}

