﻿namespace NganHang.Models
{
    public class Customer
    {
        public int CustomerID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ContactAndAddress { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public virtual ICollection<Account> Accounts { get; set; }
        public virtual ICollection<Transactions> Transactions { get; set; }

    }
}
