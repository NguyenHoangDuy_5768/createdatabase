﻿using System.ComponentModel.DataAnnotations.Schema;

namespace NganHang.Models
{
    public class Account
    {
        public int AccountID { get; set; }
        public int CustomerID { get; set; }
        public string AccountName { get; set; }
        public virtual Customer Customer { get; set; }
    }
}
